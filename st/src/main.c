
#include "Failure_Path.h"
#include <stdio.h>

int main (void)
{
	TBOOL X1, X2, X3, X4, X5, X6, X7, X8;


	X1 = 1;
	X2 = 1;
	X3 = 1;
	X4 = 1;
	X5 = 1;
	X6 = 1;
	X7 = 1;
	X8 = 1;

	/* if X1 => 9 */
	Failure_Path(X1, X2, X3, X4, X5, X6, X7, X8);

	X1 = 0;

	/* else, if X2 et X8 =>  3 */
	Failure_Path(X1, X2, X3, X4, X5, X6, X7, X8);

	X8 = 0;

	/* ... else, if !X8 et X2 => 4 */
	Failure_Path(X1, X2, X3, X4, X5, X6, X7, X8);

	X2 = 0;
	X8 = 1;

	/* ... else, else !X8 et X2, if X8 et X3 => 8*/
	Failure_Path(X1, X2, X3, X4, X5, X6, X7, X8);

	X8 = 0;
	/* ... else, else !X8 et X2, else, if !X8 et X3=> 7 */
	Failure_Path(X1, X2, X3, X4, X5, X6, X7, X8);

	X3 = 0;
	/* ... else, .. !X8 et X3, if X4 et X5 => 2 */
	Failure_Path(X1, X2, X3, X4, X5, X6, X7, X8);

	X5 = 0;
  /* 8*/
	Failure_Path(X1, X2, X3, X4, X5, X6, X7, X8);

	X4 = 0;
	X5 = 0;
	/* 8 */
	Failure_Path(X1, X2, X3, X4, X5, X6, X7, X8);

	X4 = 0;
	X5 = 1;
  /* 8 */ 
	Failure_Path(X1, X2, X3, X4, X5, X6, X7, X8);

	X8 = 1;
	/* 5 */
	Failure_Path(X1, X2, X3, X4, X5, X6, X7, X8);

	X7 = 0;
	/*6 */
	Failure_Path(X1, X2, X3, X4, X5, X6, X7, X8);
	
	X6 = 0;
	/* 0*/
	Failure_Path(X1, X2, X3, X4, X5, X6, X7, X8);

	X7 = 1;
	X6 = 0;
	/* 0*/
	Failure_Path(X1, X2, X3, X4, X5, X6, X7, X8);

	X6 = 1;
	X8 = 0;
	X7 = 1;
	/* 8 */
	Failure_Path(X1, X2, X3, X4, X5, X6, X7, X8);

	X6 = 0;
  /* 0*/
	Failure_Path(X1, X2, X3, X4, X5, X6, X7, X8);

	X6 = 1;
	X7 = 0;
	/*0 */
	Failure_Path(X1, X2, X3, X4, X5, X6, X7, X8);

	

	return 0;
}